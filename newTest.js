import http from 'k6/http'
import { sleep, check, group } from 'k6'

export const options = {
  ext: {
    loadimpact: {
        projectID: 3537921,
        name: "New Demo Test with Gitlab CI and Cloud Execution 2",
        distribution: {
            scenarioLabel1: { loadZone: "amazon:ie:dublin", percent: 40 },
            scenarioLabel2: { loadZone: "amazon:us:ashburn", percent: 33 },
            scenarioLabel3: { loadZone: "amazon:us:columbus", percent: 27 }
        },
        note: `${__ENV.MY_NOTE}`,
        apm: [],
   }
  },
  thresholds: {
    "http_req_duration": ["p(95)>=5000"],
  },
  scenarios: {
    Login: {
      executor: 'constant-vus',
      gracefulStop: '30s',
      duration: '2m30s',
      vus: 20,
      exec: 'login',
    },
    Purchasing: {
      executor: 'ramping-vus',
      gracefulStop: '30s',
      stages: [
        { target: 200, duration: '1m' },
        { target: 800, duration: '1m' },
        { target: 0, duration: '1m' },
      ],
      startVUs: 10,
      gracefulRampDown: '30s',
      exec: 'purchasing',
    },
  },
}

// Scenario: Login (executor: constant-vus)

export function login() {
  let response

  group('eCommerce - http://ecommerce.test.k6.io/', function () {
    response = http.post(
      'https://ecommerce.test.k6.io/?wc-ajax=add_to_cart',
      {
        product_id: '16',
        product_sku: 'woo-beanie',
        quantity: '1',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    check(response, { 'status equals 200': response => response.status.toString() === '200' })
    sleep(1.5)
  })

  group('Cart - https://ecommerce.test.k6.io/cart/', function () {
    response = http.get('http://ecommerce.test.k6.io/cart/', {
      headers: {
        'upgrade-insecure-requests': '1',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
      },
    })
    sleep(0.8)

    response = http.get(
      'https://fonts.gstatic.com/s/sourcesanspro/v19/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7lujVj9w.woff2',
      {
        headers: {
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
          origin: 'http://ecommerce.test.k6.io',
          'sec-ch-ua-mobile': '?0',
          'sec-ch-ua-platform': '"macOS"',
          accept: '*/*',
          'x-client-data': 'CJW2yQEIpLbJAQjEtskBCKmdygEIxtLKAQiTocsBCJehywEInvnLAQjmhMwBCM+izAE=',
          'sec-fetch-site': 'cross-site',
          'sec-fetch-mode': 'cors',
          'sec-fetch-dest': 'font',
          'accept-encoding': 'gzip, deflate, br',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )

    response = http.get(
      'https://fonts.gstatic.com/s/sourcesanspro/v19/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu3cOWxw.woff2',
      {
        headers: {
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
          origin: 'http://ecommerce.test.k6.io',
          'sec-ch-ua-mobile': '?0',
          'sec-ch-ua-platform': '"macOS"',
          accept: '*/*',
          'x-client-data': 'CJW2yQEIpLbJAQjEtskBCKmdygEIxtLKAQiTocsBCJehywEInvnLAQjmhMwBCM+izAE=',
          'sec-fetch-site': 'cross-site',
          'sec-fetch-mode': 'cors',
          'sec-fetch-dest': 'font',
          'accept-encoding': 'gzip, deflate, br',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )

    response = http.get(
      'https://fonts.gstatic.com/s/sourcesanspro/v19/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu3cOWxw.woff2',
      {
        headers: {
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
          origin: 'http://ecommerce.test.k6.io',
          'sec-ch-ua-mobile': '?0',
          'sec-ch-ua-platform': '"macOS"',
          accept: '*/*',
          'x-client-data': 'CJW2yQEIpLbJAQjEtskBCKmdygEIxtLKAQiTocsBCJehywEInvnLAQjmhMwBCM+izAE=',
          'sec-fetch-site': 'cross-site',
          'sec-fetch-mode': 'cors',
          'sec-fetch-dest': 'font',
          'accept-encoding': 'gzip, deflate, br',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(3.1)

    response = http.post(
      'https://ecommerce.test.k6.io/cart/',
      {
        _wp_http_referer: '/cart/',
        'cart[c74d97b01eae257e44aa9d5bade97baf][qty]': '3',
        coupon_code: '',
        update_cart: 'Update Cart',
        'woocommerce-cart-nonce': '8409292423',
      },
      {
        headers: {
          accept: 'text/html, */*; q=0.01',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(1.6)

    response = http.post(
      'https://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
      {
        time: '1650903729810',
      },
      {
        headers: {
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(1.5)
  })

  group('Checkout - http://ecommerce.test.k6.io/checkout/', function () {
    response = http.get('http://ecommerce.test.k6.io/checkout/', {
      headers: {
        'upgrade-insecure-requests': '1',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
      },
    })
    sleep(1.1)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=update_order_review',
      {
        address: '',
        address_2: '',
        city: '',
        country: 'US',
        has_full_address: 'false',
        payment_method: 'cod',
        post_data:
          'billing_first_name=&billing_last_name=&billing_company=&billing_country=US&billing_address_1=&billing_address_2=&billing_city=&billing_state=CO&billing_postcode=&billing_phone=&billing_email=&order_comments=&payment_method=cod&woocommerce-process-checkout-nonce=26727df04d&_wp_http_referer=%2Fcheckout%2F',
        postcode: '',
        s_address: '',
        s_address_2: '',
        s_city: '',
        s_country: 'US',
        s_postcode: '',
        s_state: 'CO',
        security: '23a3cf2212',
        state: 'CO',
      },
      {
        headers: {
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(15.1)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=update_order_review',
      {
        address: '101 main st',
        address_2: '',
        city: 'midtoqn',
        country: 'US',
        has_full_address: 'true',
        payment_method: 'cod',
        post_data:
          'billing_first_name=Bill&billing_last_name=Rainaud&billing_company=&billing_country=US&billing_address_1=101%20main%20st&billing_address_2=&billing_city=midtoqn&billing_state=CO&billing_postcode=07789&billing_phone=&billing_email=&order_comments=&payment_method=cod&woocommerce-process-checkout-nonce=26727df04d&_wp_http_referer=%2F%3Fwc-ajax%3Dupdate_order_review',
        postcode: '07789',
        s_address: '101 main st',
        s_address_2: '',
        s_city: 'midtoqn',
        s_country: 'US',
        s_postcode: '07789',
        s_state: 'CO',
        security: '23a3cf2212',
        state: 'CO',
      },
      {
        headers: {
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(11)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=checkout',
      {
        _wp_http_referer: '/?wc-ajax=update_order_review',
        billing_address_1: '101 main st',
        billing_address_2: '',
        billing_city: 'midtoqn',
        billing_company: '',
        billing_country: 'US',
        billing_email: 'bill@K6.COM',
        billing_first_name: 'Bill',
        billing_last_name: 'Rainaud',
        billing_phone: '555-555-5555',
        billing_postcode: '07789',
        billing_state: 'CO',
        order_comments: '',
        payment_method: 'cod',
        'woocommerce-process-checkout-nonce': '26727df04d',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(3.8)
  })

  group(
    'page_4 - http://ecommerce.test.k6.io/checkout/order-received/7898/?key=wc_order_wdRhQAxKktTbW',
    function () {
      response = http.get(
        'http://ecommerce.test.k6.io/checkout/order-received/7898/?key=wc_order_wdRhQAxKktTbW',
        {
          headers: {
            'upgrade-insecure-requests': '1',
            accept:
              'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
          },
        }
      )
      sleep(1.3)

      response = http.post(
        'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
        {
          time: '1650903768242',
        },
        {
          headers: {
            accept: '*/*',
            'x-requested-with': 'XMLHttpRequest',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            origin: 'http://ecommerce.test.k6.io',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
          },
        }
      )
    }
  )
}

// Scenario: Purchasing (executor: ramping-vus)

export function purchasing() {
  let response

  group('eCommerce - http://ecommerce.test.k6.io/', function () {
    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=add_to_cart',
      {
        product_id: '16',
        product_sku: 'woo-beanie',
        quantity: '1',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(1.5)
  })

  group('Cart - http://ecommerce.test.k6.io/cart/', function () {
    response = http.get('http://ecommerce.test.k6.io/cart/', {
      headers: {
        'upgrade-insecure-requests': '1',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
      },
    })
    sleep(0.8)

    response = http.get(
      'https://fonts.gstatic.com/s/sourcesanspro/v19/6xK3dSBYKcSV-LCoeQqfX1RYOo3qOK7lujVj9w.woff2',
      {
        headers: {
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
          origin: 'http://ecommerce.test.k6.io',
          'sec-ch-ua-mobile': '?0',
          'sec-ch-ua-platform': '"macOS"',
          accept: '*/*',
          'x-client-data': 'CJW2yQEIpLbJAQjEtskBCKmdygEIxtLKAQiTocsBCJehywEInvnLAQjmhMwBCM+izAE=',
          'sec-fetch-site': 'cross-site',
          'sec-fetch-mode': 'cors',
          'sec-fetch-dest': 'font',
          'accept-encoding': 'gzip, deflate, br',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )

    response = http.get(
      'https://fonts.gstatic.com/s/sourcesanspro/v19/6xKydSBYKcSV-LCoeQqfX1RYOo3i54rwlxdu3cOWxw.woff2',
      {
        headers: {
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
          origin: 'http://ecommerce.test.k6.io',
          'sec-ch-ua-mobile': '?0',
          'sec-ch-ua-platform': '"macOS"',
          accept: '*/*',
          'x-client-data': 'CJW2yQEIpLbJAQjEtskBCKmdygEIxtLKAQiTocsBCJehywEInvnLAQjmhMwBCM+izAE=',
          'sec-fetch-site': 'cross-site',
          'sec-fetch-mode': 'cors',
          'sec-fetch-dest': 'font',
          'accept-encoding': 'gzip, deflate, br',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )

    response = http.get(
      'https://fonts.gstatic.com/s/sourcesanspro/v19/6xKydSBYKcSV-LCoeQqfX1RYOo3ik4zwlxdu3cOWxw.woff2',
      {
        headers: {
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"',
          origin: 'http://ecommerce.test.k6.io',
          'sec-ch-ua-mobile': '?0',
          'sec-ch-ua-platform': '"macOS"',
          accept: '*/*',
          'x-client-data': 'CJW2yQEIpLbJAQjEtskBCKmdygEIxtLKAQiTocsBCJehywEInvnLAQjmhMwBCM+izAE=',
          'sec-fetch-site': 'cross-site',
          'sec-fetch-mode': 'cors',
          'sec-fetch-dest': 'font',
          'accept-encoding': 'gzip, deflate, br',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(3.1)

    response = http.post(
      'http://ecommerce.test.k6.io/cart/',
      {
        _wp_http_referer: '/cart/',
        'cart[c74d97b01eae257e44aa9d5bade97baf][qty]': '3',
        coupon_code: '',
        update_cart: 'Update Cart',
        'woocommerce-cart-nonce': '8409292423',
      },
      {
        headers: {
          accept: 'text/html, */*; q=0.01',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(1.6)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
      {
        time: '1650903729810',
      },
      {
        headers: {
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(1.5)
  })

  group('Checkout - http://ecommerce.test.k6.io/checkout/', function () {
    response = http.get('http://ecommerce.test.k6.io/checkout/', {
      headers: {
        'upgrade-insecure-requests': '1',
        accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'accept-encoding': 'gzip, deflate',
        'accept-language': 'en-US,en;q=0.9',
      },
    })
    sleep(1.1)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=update_order_review',
      {
        address: '',
        address_2: '',
        city: '',
        country: 'US',
        has_full_address: 'false',
        payment_method: 'cod',
        post_data:
          'billing_first_name=&billing_last_name=&billing_company=&billing_country=US&billing_address_1=&billing_address_2=&billing_city=&billing_state=CO&billing_postcode=&billing_phone=&billing_email=&order_comments=&payment_method=cod&woocommerce-process-checkout-nonce=26727df04d&_wp_http_referer=%2Fcheckout%2F',
        postcode: '',
        s_address: '',
        s_address_2: '',
        s_city: '',
        s_country: 'US',
        s_postcode: '',
        s_state: 'CO',
        security: '23a3cf2212',
        state: 'CO',
      },
      {
        headers: {
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(15.1)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=update_order_review',
      {
        address: '101 main st',
        address_2: '',
        city: 'midtoqn',
        country: 'US',
        has_full_address: 'true',
        payment_method: 'cod',
        post_data:
          'billing_first_name=Bill&billing_last_name=Rainaud&billing_company=&billing_country=US&billing_address_1=101%20main%20st&billing_address_2=&billing_city=midtoqn&billing_state=CO&billing_postcode=07789&billing_phone=&billing_email=&order_comments=&payment_method=cod&woocommerce-process-checkout-nonce=26727df04d&_wp_http_referer=%2F%3Fwc-ajax%3Dupdate_order_review',
        postcode: '07789',
        s_address: '101 main st',
        s_address_2: '',
        s_city: 'midtoqn',
        s_country: 'US',
        s_postcode: '07789',
        s_state: 'CO',
        security: '23a3cf2212',
        state: 'CO',
      },
      {
        headers: {
          accept: '*/*',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(11)

    response = http.post(
      'http://ecommerce.test.k6.io/?wc-ajax=checkout',
      {
        _wp_http_referer: '/?wc-ajax=update_order_review',
        billing_address_1: '101 main st',
        billing_address_2: '',
        billing_city: 'midtoqn',
        billing_company: '',
        billing_country: 'US',
        billing_email: 'bill@K6.COM',
        billing_first_name: 'Bill',
        billing_last_name: 'Rainaud',
        billing_phone: '555-555-5555',
        billing_postcode: '07789',
        billing_state: 'CO',
        order_comments: '',
        payment_method: 'cod',
        'woocommerce-process-checkout-nonce': '26727df04d',
      },
      {
        headers: {
          accept: 'application/json, text/javascript, */*; q=0.01',
          'x-requested-with': 'XMLHttpRequest',
          'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
          origin: 'http://ecommerce.test.k6.io',
          'accept-encoding': 'gzip, deflate',
          'accept-language': 'en-US,en;q=0.9',
        },
      }
    )
    sleep(3.8)
  })

  group('Order Confirmation - http://ecommerce.test.k6.io/checkout/order-received/7898/?key=wc_order_wdRhQAxKktTbW',
    function () {
      response = http.get(
        'http://ecommerce.test.k6.io/checkout/order-received/7898/?key=wc_order_wdRhQAxKktTbW',
        {
          headers: {
            'upgrade-insecure-requests': '1',
            accept:
              'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
          },
        }
      )
      sleep(1.3)

      response = http.post(
        'http://ecommerce.test.k6.io/?wc-ajax=get_refreshed_fragments',
        {
          time: '1650903768242',
        },
        {
          headers: {
            accept: '*/*',
            'x-requested-with': 'XMLHttpRequest',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            origin: 'http://ecommerce.test.k6.io',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'en-US,en;q=0.9',
          },
        }
      )
  })
}
