import {sleep} from 'k6';
import http from 'k6/http';

export let options = {
    duration: '30s',
    vus: 50,
    thresholds: {
        http_req_duration: ['p(95)>500']
    },
    note: "Hello Team"
};

export default function () {
    http.get('http://test.k6.io/contacts.php');
    sleep(3);
}
